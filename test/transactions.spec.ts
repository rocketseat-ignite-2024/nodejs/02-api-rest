import { execSync } from 'node:child_process'
import { afterAll, beforeAll, beforeEach, describe, expect, it } from 'vitest'
import request from 'supertest'

import { app } from '../src/app'

describe('Transactions routes', () => {
  beforeAll(async () => await app.ready())

  afterAll(async () => app.close())

  beforeEach(() => {
    execSync('npm run knex migrate:rollback --all')
    execSync('npm run knex migrate:latest')
  })

  it('should be able to create a new transaction', async () => {
    await request(app.server)
      .post('/transactions')
      .send({
        title: 'New credit transaction',
        amount: 100,
        type: 'credit',
      })
      .expect(201)
  })

  it('should be able to list all transactions', async () => {
    const createResponse = await request(app.server)
      .post('/transactions')
      .send({
        title: 'New credit transaction',
        amount: 100,
        type: 'credit',
      })

    const cookies = createResponse.get('Set-Cookie')

    const getResponse = await request(app.server)
      .get('/transactions')
      .set('Cookie', cookies ?? [])
      .expect(200)

    expect(getResponse.body.transactions).toEqual([
      expect.objectContaining({
        title: 'New credit transaction',
        amount: 100,
      }),
    ])
  })

  it('should be able to get a specific transaction', async () => {
    const createResponse = await request(app.server)
      .post('/transactions')
      .send({
        title: 'New credit transaction',
        amount: 100,
        type: 'credit',
      })

    const cookies = createResponse.get('Set-Cookie')

    const listTransactionsResponse = await request(app.server)
      .get('/transactions')
      .set('Cookie', cookies ?? [])
      .expect(200)

    const { id: transactionId } = listTransactionsResponse.body.transactions[0]

    const transactionResponse = await request(app.server)
      .get(`/transactions/${transactionId}`)
      .set('Cookie', cookies ?? [])
      .expect(200)

    expect(transactionResponse.body.transaction).toEqual(
      expect.objectContaining({
        title: 'New credit transaction',
        amount: 100,
      }),
    )
  })

  it('should be able to get the summary', async () => {
    const createResponse = await request(app.server)
      .post('/transactions')
      .send({
        title: 'New credit transaction',
        amount: 100,
        type: 'credit',
      })

    const cookies = createResponse.get('Set-Cookie')!

    await request(app.server)
      .post('/transactions')
      .set('Cookie', cookies)
      .send({
        title: 'New debit transaction',
        amount: 20,
        type: 'debit',
      })

    const response = await request(app.server)
      .get('/transactions/summary')
      .set('Cookie', cookies ?? [])
      .expect(200)

    expect(response.body.summary).toEqual({ amount: 80 })
  })
})
