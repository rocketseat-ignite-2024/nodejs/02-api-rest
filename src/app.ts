import fastify from 'fastify'
import cookie from '@fastify/cookie'

import { env } from './env'
import { transactionsRoutes } from './routes/transactions'

export const app = fastify()

app.register(cookie)

// Global Hook, in case of hooks for specifics routes create .addHook on routes files
app.addHook('preHandler', async (request) => {
  if (env.NODE_ENV === 'development')
    console.log(`[${request.method}] ${request.url}`)
})

app.register(transactionsRoutes, { prefix: 'transactions' })
