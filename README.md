# RF

- [x] 0 usuário deve poder criar uma nova transação;
- [ ] 0 usuário deve poder obter um resumo da sua conta;
- [x] 0 usuário deve poder listar todas transações que já ocorreram;
- [x] 0 usuário deve poder visualizar uma transação única;

# RN

- [x] A transação pode ser do tipo crédito que somará ao valor total, ou débito subtrairá;
- [ ] Deve ser possível identificarmos o usuário entre as requisições;
- [ ] 0 usuário só pode visualizar transações o qual ele criou;
